﻿namespace Knock.Api.Models
{
    /// <summary>
    /// Triangle Type
    /// </summary>
    public enum TriangleType
    {
        /// <summary>
        /// Error
        /// </summary>
        Error,
        /// <summary>
        /// Isosceles
        /// </summary>
        Isosceles,
        /// <summary>
        /// Equilateral
        /// </summary>
        Equilateral,
        /// <summary>
        /// Scalene
        /// </summary>
        Scalene
    }
}