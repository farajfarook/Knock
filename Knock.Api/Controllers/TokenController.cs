﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Knock.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [Produces("application/json", "text/json")]    
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Your Token.
        /// </summary>        
        /// <returns></returns>
        [HttpGet]        
        [SwaggerOperation("Token_Get")]        
        public string Token()
        {
            var token = _configuration.GetValue<string>("Token");
            return token;
        }
    }
}