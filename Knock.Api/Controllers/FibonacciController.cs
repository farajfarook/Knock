﻿using System;
using Knock.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Knock.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [Produces("application/json", "text/json")]   
    public class FibonacciController : Controller
    {
        private readonly INumberService _numberService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberService"></param>
        public FibonacciController(INumberService numberService)
        {
            _numberService = numberService;
        }

        /// <summary>
        /// Returns the nth fibonacci number.
        /// </summary>                        
        /// <param name="n">The index (n) of the fibonacci sequence</param>        
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpGet]
        [SwaggerOperation("Fibonacci_Get")]
        public long Fibonacci(long n)
        {
            if (!ModelState.IsValid) throw new Exception("The request is invalid.");
            return _numberService.GetFibonacci(n);
        }
    }
}