﻿using System;
using System.Linq;
using Knock.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Knock.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]    
    [Produces("application/json", "text/json")]    
    public class ReverseWordsController : Controller
    {
        private readonly IWordService _wordService;

        /// <inheritdoc />
        public ReverseWordsController(IWordService wordService)
        {
            _wordService = wordService;
        }

        /// <summary>
        /// Reverses the letters of each word in a sentence.
        /// </summary>
        /// <param name="sentence">A sentence</param>
        /// <returns></returns>
        [HttpGet]        
        [SwaggerOperation("ReverseWords_Get")]
        public string Index(string sentence)
        {
            if (!ModelState.IsValid) throw new Exception("The request is invalid.");
            return _wordService.Reverse(sentence);
        }
    }
}