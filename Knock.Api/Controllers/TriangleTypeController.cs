﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using Knock.Api.Models;
using Knock.Api.Services;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Knock.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]    
    [Produces("application/json", "text/json")]    
    public class TriangleTypeController : Controller
    {
        private readonly ITriangleService _triangleService;

        /// <inheritdoc />
        public TriangleTypeController(ITriangleService triangleService)
        {
            _triangleService = triangleService;
        }

        /// <summary>
        /// Returns the type of triange given the lengths of its sides.
        /// </summary>
        /// <param name="a">The length of side a</param>
        /// <param name="b">The length of side b</param>
        /// <param name="c">The length of side c</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerOperation("TriangleType_Get")]
        public string Index(int a, int b, int c)
        {
            if (!ModelState.IsValid) throw new Exception("The request is invalid.");
            return _triangleService.GetTriangleType(a, b, c).ToString();
        }
    }
}