﻿using Knock.Api.Models;

namespace Knock.Api.Services
{
    /// <summary>
    /// Triangle service
    /// </summary>
    public interface ITriangleService
    {
        /// <summary>
        /// Get Triangle type
        /// </summary>
        /// <param name="sideA"></param>
        /// <param name="sideB"></param>
        /// <param name="sideC"></param>
        /// <returns></returns>
        TriangleType GetTriangleType(int sideA, int sideB, int sideC);
    }
}