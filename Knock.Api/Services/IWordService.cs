﻿namespace Knock.Api.Services
{
    /// <summary>
    /// Word service
    /// </summary>
    public interface IWordService
    {
        /// <summary>
        /// Reverse string
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns></returns>
        string Reverse(string sentence);
    }
}