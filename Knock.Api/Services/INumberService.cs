﻿namespace Knock.Api.Services
{
    /// <summary>
    /// Number services
    /// </summary>
    public interface INumberService
    {
        /// <summary>
        /// Get nth fibonachi
        /// </summary>
        /// <param name="nth"></param>
        /// <param name="checkThreshold"></param>
        /// <returns></returns>
        long GetFibonacci(long nth, bool checkThreshold = true);
    }
}