﻿using System;
using System.Collections.Generic;
using System.Linq;
using Knock.Api.Models;
using Microsoft.Extensions.Caching.Memory;

namespace Knock.Api.Services
{
    /// <summary>
    /// Knock services
    /// </summary>
    public class KnockService: INumberService, IWordService, ITriangleService
    {
        private const long Threshold = 92;
        private readonly IMemoryCache _memoryCache;

        /// <inheritdoc />
        public KnockService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }


        /// <inheritdoc />
        public long GetFibonacci(long n, bool checkThreshold = true)
        {
            if (checkThreshold && (n > Threshold || n < -Threshold))
            {
                throw new ArgumentOutOfRangeException(nameof(n), $"Value cannot exceed {Threshold}.");
            }

            var key = $"fibonacci-{n}";
            if(_memoryCache.TryGetValue(key, out long value))
            {
                return value;
            }

            if (n < 0)
            {
                n = n * -1;
                value = (int) Math.Pow(-1, n + 1) * GetFibonacci(n, false);
            }
            else
            {
                if (n == 0 || n == 1) return n;
                value = GetFibonacci(n - 2, false) + GetFibonacci(n - 1, false);
            }
            _memoryCache.Set(key, value);
            return value;
        }

        /// <inheritdoc />
        public string Reverse(string sentence)
        {
            var key = $"reverse-{sentence}";
            if(_memoryCache.TryGetValue(key, out string value))
            {
                return value;
            }
            var words = sentence.Split(' ');
            var reverseWords = new List<string>();
            foreach (var word in words)
            {
                var wordKey = $"reverse-{word}";
                if(!_memoryCache.TryGetValue(wordKey, out string reverseWord))
                {
                    reverseWord = new string(word.ToCharArray().Reverse().ToArray());
                    _memoryCache.Set(wordKey, reverseWord);
                }
                reverseWords.Add(reverseWord);
            }
            
            value = string.Join(' ', reverseWords);
            _memoryCache.Set(key, value);
            return value;
        }

        /// <inheritdoc />
        public TriangleType GetTriangleType(int sideA, int sideB, int sideC)
        {
            var key = $"triangle-type-{sideA}-{sideB}-{sideC}";
            if(_memoryCache.TryGetValue(key, out TriangleType value))
            {
                return value;
            }
            var valid = ValidateTrangle(sideA, sideB, sideC);
            if (valid)
            {
                var aEqB = sideA == sideB;
                var bEqC = sideB == sideC;
                var cEqA = sideC == sideA;
                
                if (aEqB && bEqC)
                {
                    value = TriangleType.Equilateral;
                }
                else if (aEqB || bEqC || cEqA)
                {
                    value = TriangleType.Scalene;
                }
                else
                {
                    value = TriangleType.Isosceles;
                }
            }
            else
            {
                value = TriangleType.Error;
            }
            _memoryCache.Set(key, value);
            return value;
        }

        /// <summary>
        /// Validate according to Triangle validation theorem 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        private static bool ValidateTrangle(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0) return false;
            return ((a + b) > c && (b + c) > a && (c + a) > b);
        }
    }
}